FROM node:10 AS build
WORKDIR /src

COPY . .

RUN npm i
RUN npm run build

FROM nginx
WORKDIR /publish
COPY --from=build ./src/build /usr/share/nginx/html
