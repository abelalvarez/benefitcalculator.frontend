const axios = require('axios');

export const createQuote = async (customer) => {
    const { data } = await axios.post(`${process.env.REACT_APP_URI}/api/Benefits/quote`, customer);
    return data;
}