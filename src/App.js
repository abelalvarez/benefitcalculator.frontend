import './App.css';
import Quote from './components/Quote';

function App() {
  return (
    <div className="container">
      <div className="row">
        <Quote />
      </div>
    </div>
  );
}

export default App;
