import { useContext } from "react";

import QuoteContext from "../context/QuoteContext";
import Dependent from './Dependent';
import Employee from './Employee';

const QuoteForm = () => {
    const { quote } = useContext(QuoteContext);
    const { handleClick, handleSubmit } = quote;

    return (
        <form className="">
            <div>
                <div className="employee-section">
                    <h3 className="h5">Employee Section</h3>
                    <Employee />
                </div>
                <div className="dependent-section">
                    <h3 className="h5">Dependent Section
                        <button type="button" className="btn btn-primary btn-circle btn-sm" onClick={handleClick}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-plus-lg" viewBox="0 0 16 16">
                                <path d="M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z" />
                            </svg>
                        </button>
                    </h3>
                    <Dependent />
                </div>

                <div className="text-center">
                    <button type="submit" className="btn btn-success btn-lg submit-quote" onClick={handleSubmit}>Submit Quote</button>
                </div>
            </div>
        </form>
    );
};

export default QuoteForm;