import React, { useContext } from 'react';
import Customer from './Customer';
import QuoteContext from './../context/QuoteContext';


const Employee = () => {
    const { quote } = useContext(QuoteContext);
    const { employee, setEmployee } = quote;

    const handleChange = (e) => {
        const { name, value, type } = e.target;
        const currentEmployee = { ...employee };

        if (type.toLowerCase() === 'checkbox') {

        }
        else {
            currentEmployee[name] = value;
        }

        setEmployee(currentEmployee);
    }

    return (
        <div className="row center">
            <Customer handleChange={handleChange} customer={employee} isRequired={true} id="employee" />
        </div>);

}

export default Employee;