import './Customer.css';

const Customer = ({ customer, isRequired, id, index, handleChange, handleRemove }) => {
    return (<>
        <div className="col-3">
            <label>First name</label>
            <input name="firstName" value={customer.first} required={isRequired} id={`${id}-first`} className="form-control" type="text" onChange={e => handleChange(e, index)} placeholder="First name" />
        </div>
        <div className="col-3">
            <label>Middle name</label>
            <input name="middleName" value={customer.middle} id={`${id}-middle`} className="form-control" type="text" onChange={e => handleChange(e, index)} placeholder="Middle name" />
        </div>
        <div className="col-3">
            <label>Last name</label>
            <input name="lastName" className="form-control" value={customer.last} required={isRequired} id={`${id}-last`} type="text" onChange={e => handleChange(e, index)} placeholder="Last name" />
        </div>
        <div className="col-3">
            <span>
                <label className="include-checkbox">
                    <input name="includeInBenefits" type="checkbox" disabled={isRequired} id={`${id}-checkbox`} checked={customer.includeInBenefits} onChange={e => handleChange(e, index)} />
                    <span> Include in Quote</span>
                </label>
                <RemoveButton isRequired={isRequired} index={index} handleRemove={handleRemove} />
            </span>
        </div>
    </>);
}

const RemoveButton = ({ isRequired, index, handleRemove }) => {
    return !isRequired && (
        <button type="button" onClick={e => handleRemove(e, index)} className="btn btn-danger"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill" viewBox="0 0 16 16">
            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z" />
        </svg></button>);
}

export default Customer;