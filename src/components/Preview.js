import { useContext } from "react"
import QuoteContext from "../context/QuoteContext"

const Preview = () => {
    const { preview } = useContext(QuoteContext);
    const { previewData } = preview;

    if (previewData && previewData.employee) {
        return (<>
            <h2>Preview Quote</h2>
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Full Name</th>
                            <th scope="col">Annual Benefit Cost</th>
                            <th scope="col">After Discounts</th>
                            <th scope="col">Due Every Paycheck</th>
                        </tr>
                    </thead>
                    <tbody>
                        <TableData data={previewData} />
                    </tbody>
                </table>
                <div>
                    <p className="h4">Total Deductions: ${previewData.deductions}</p>
                    <p className="h4">Paycheck: ${previewData.payCheck}</p>
                </div>
            </div>
        </>)
    }
    return (<></>);
}

const TableData = ({ data }) => {
    if (data && data.employee) {

        const results = [];
        results.push(<tr key={Date.now().toString()}>
            <td>{data.employee.fullName}</td>
            <td>${data.employee.benefit.annualCost}</td>
            <td>${data.employee.benefit.annualCostAfterDiscounts}</td>
            <td>${data.employee.benefit.paycheckCost}</td>
        </tr>);
        data.dependents.forEach((item, index) => {
            results.push(<tr key={index}>
                <td>{item.fullName}</td>
                <td>${item.benefit.annualCost}</td>
                <td>${item.benefit.annualCostAfterDiscounts}</td>
                <td>${item.benefit.paycheckCost}</td>
            </tr>);
        });

        return results;
    }
    return (<></>)
}

export default Preview;