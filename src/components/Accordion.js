import React, { useState } from 'react';

const Accordion = ({ title, Component }) => {
    const [expand, setExpand] = useState(true);

    return (<>
        <div className="accordion" >
            <div className="accordion-item">
                <h2 className="accordion-header" id="headingOne">
                    <button className={`accordion-button ${expand ? 'collapsed' : ''}`} type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" onClick={() => setExpand(!expand)}>
                        <span className="h5">{title}</span>
                    </button>
                </h2>
                <div id="collapseOne" className={`accordion-collapse collapse ${expand ? 'show' : 'hide'}`} aria-labelledby="headingOne" >
                    <div className="accordion-body">
                        <Component />
                    </div>
                </div>
            </div>
        </div>
    </ >)
};


export default Accordion;