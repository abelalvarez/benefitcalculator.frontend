import React, { useState } from 'react';

import { QuoteProvider } from './../context/QuoteContext';
import QuoteForm from './QuoteForm';
import Preview from './Preview';
import Accordion from './Accordion';

import { createQuote } from './../services/benefitService';

import './Quote.css';

const Quote = () => {
    const customer = { firstName: '', middleName: '', lastName: '', includeInBenefits: true };
    const handleSubmit = async (e) => {
        e.preventDefault();
        employee.dependents = dependents.filter(element => element.includeInBenefits);

        const data = await createQuote(employee);
        setPreviewData(data);
    }

    const handleClick = () => {
        setDependent([...dependents, { ...customer }]);
    }

    const [employee, setEmployee] = useState({ ...customer });
    const [dependents, setDependent] = useState([]);
    const [previewData, setPreviewData] = useState([]);

    const context = {
        quote: {
            employee: employee,
            setEmployee: setEmployee,
            dependents: dependents,
            setDependent: setDependent,
            handleClick: handleClick,
            handleSubmit: handleSubmit,
        },
        preview: {
            previewData: previewData,
            setPreviewData: setPreviewData,
        }
    };

    return (
        <QuoteProvider value={context}>
            <h1 className="h2">Benefits Quote</h1>
            <Accordion title="Benefit Questionnaire" Component={QuoteForm} />
            <hr />
            <Accordion title="Preview Paycheck" Component={Preview} />
        </QuoteProvider>
    );
}

export default Quote;