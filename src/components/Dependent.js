import React, { useContext } from 'react';

import Customer from './Customer';
import QuoteContext from '../context/QuoteContext';

import './Dependent.css';

const Dependent = () => {
    const { quote } = useContext(QuoteContext);
    const { dependents, setDependent } = quote;

    const handleChange = (e, index) => {
        const { name, value, type } = e.target;
        const currentList = [...dependents];
        
        if (type.toLowerCase() === 'checkbox') {
            currentList[index][name] = !currentList[index][name];
        }
        else {
            currentList[index][name] = value
        }

        setDependent(currentList);
    }
    const handleRemove = (e, index) => {
        e.preventDefault();

        const currentList = [...dependents];
        currentList.splice(index, 1);

        setDependent(currentList);
    }

    return dependents.map((element, index) => (
        <div className="row center" key={index}>
            <Customer handleChange={handleChange} handleRemove={handleRemove} customer={element} id={index} index={index} />
        </div>)
    );
}

export default Dependent;